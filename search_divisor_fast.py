#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Compute F_a + ... + F_b and search some prime divisors.

Similar than search_divisor_fast.py
but don't compute the exact sum
and give less information.

Usage: ./search_divisor_fast.py a b

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: October 12, 2020
"""

import sys

import search_divisor


def main() -> None:
    """
    Get command line parameters, compute and print prime divisors.
    """
    a, b = map(int, sys.argv[1:3])
    print('F_{} + ... + F_{}'.format(a, b))
    sys.stdout.flush()

    print('Search...')
    sys.stdout.flush()

    for prime in search_divisor.PRIME_16_ARRAY:
        f_sum_mod = search_divisor.fibonacci_sum(a, b, modulo=prime)
        if f_sum_mod == 0:
            print('divisible by {}'.format(prime))
            sys.stdout.flush()


if __name__ == '__main__':
    main()
