/* -*- coding: latin-1 -*- */
/*
  Determine if some range of consecutive Fibonacci numbers are divisible.

  Read nb from standard input,
  then for each a b d from standard input
  determine if F_a + F_{a+1} + F_{a+2} + ... + F_b is divisible by d.

  GPLv3 --- Copyright (C) 2020, 2021 Olivier Pirson
  Olivier Pirson --- http://www.opimedia.be/
  January 9, 2021
*/

#include <assert.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>


static uint64_t square(uint64_t n, uint64_t modulo) {
  assert(modulo >= 1);

  n %= modulo;

  return (n * n) % modulo;
}


static void fibonacci_pair(uint64_t n, uint64_t modulo,
                           uint64_t* f_n_1, uint64_t* f_n) {
  assert(modulo >= 1);
  assert(f_n_1 != NULL);
  assert(f_n != NULL);

  if (n <= 1) {
    *f_n_1 = (n == 0
              ? 1
              : 0);
    *f_n = n;
  }
  else {
    const uint64_t q = n / 2;

    uint64_t f_q_1;
    uint64_t f_q;

    fibonacci_pair(q, modulo, &f_q_1, &f_q);

    if ((n & 1) == 0) {
      /*
        n = 2q
        q = n/2
        F_{n-1} = F_q^2 - F_{q-1}^2
        F_n = F_q (2 F_{q-1} + F_q)
      */
      *f_n_1 = square(f_q, modulo) + square(f_q_1, modulo);
      *f_n = f_q * ((f_q_1 * 2 % modulo + f_q) % modulo);
    }
    else {
      /*
        n = 2q + 1
        q = (n - 1)/2
        F_{n-1} = F_q (2 F_{q-1} + F_q)
        F_n = F_{q+1}^2 - F_q^2 = (F_q + F_{q-1})^2 - F_q^2
      */
      *f_n_1 = f_q * ((f_q_1 * 2 % modulo + f_q) % modulo);
      *f_n = square(f_q + f_q_1, modulo) + square(f_q, modulo);
    }
  }

  *f_n_1 %= modulo;
  *f_n %= modulo;
}


int main(void) {
  unsigned int nb;
  scanf("%u", &nb);

  for (unsigned int i = 0; i < nb; ++i) {
    // Read inputs
    uint64_t a;
    uint64_t b;
    uint64_t d;
    scanf("%" SCNu64 "%" SCNu64 "%" SCNu64, &a, &b, &d);

    // Compute F_{a-1} and F_a
    uint64_t f_n_1;
    uint64_t f_n;

    fibonacci_pair(a, d, &f_n_1, &f_n);

    // Compute sum (F_a + F_{a+1} + F_{a+2} + ... + F_b) modulo d
    uint64_t sum_mod = f_n;

    for (uint64_t j = a + 1; j <= b; ++j) {
      const uint64_t tmp = f_n_1;

      f_n_1 = f_n;
      f_n = (f_n + tmp) % d;

      sum_mod = (sum_mod + f_n) % d;
    }

    // Print result
    printf("F_%" PRIu64 " + ... + F_%" PRIu64 " is %sdivisible by %" PRIu64 "\n", a, b,
           (sum_mod == 0
            ? ""
            : "NOT "), d);
    fflush(stdout);
  }

  return EXIT_SUCCESS;
}
