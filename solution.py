#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Determine if some range of consecutive Fibonacci numbers are divisible.

Read nb from standard input,
then for each a b d from standard input
     determine if F_a + F_{a+1} + F_{a+2} + ... + F_b is divisible by d.

:license: GPLv3 --- Copyright (C) 2020, 2021 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: January 9, 2021
"""

import sys

from typing import Dict, Tuple


MEMO: Dict[Tuple[int, int], Tuple[int, int]] = dict()


def fibonacci_pair(n: int, modulo: int) -> Tuple[int, int]:
    """
    Return the pair (F_{n-1} % modulo, F_n % modulo).

    :param n: int >= 0
    :param modulo: (int >= 1)

    :return: (int >= 0, int >= 0)
    """
    assert n >= 0
    assert modulo >= 1

    if n <= 1:
        f_n_1 = abs(n - 1)
        f_n = n
    else:
        q = n // 2
        f_q_1, f_q = fibonacci_pair(q, modulo)

        if n % 2 == 0:
            # n = 2q
            # q = n/2
            # F_{n-1} = F_q^2 - F_{q-1}^2
            # F_n = F_q (2 F_{q-1} + F_q)
            f_n_1 = (f_q**2 % modulo) + (f_q_1**2 % modulo)
            f_n = f_q * ((f_q_1 * 2 % modulo + f_q) % modulo)
        else:
            # n = 2q + 1
            # q = (n - 1)/2
            # F_{n-1} = F_q (2 F_{q-1} + F_q)
            # F_n = F_{q+1}^2 - F_q^2 = (F_q + F_{q-1})^2 - F_q^2
            f_n_1 = f_q * ((f_q_1 * 2 % modulo + f_q) % modulo)
            f_n = (((f_q + f_q_1) % modulo)**2 % modulo) + (f_q**2 % modulo)

    return f_n_1 % modulo, f_n % modulo


def fibonacci_pair_iteration(n: int, modulo: int) -> Tuple[int, int]:
    """
    Return the pair (F_{n-1} % modulo, F_n % modulo).

    Same that fibonacci_pair() but in less efficient way.

    :param n: int >= 0
    :param modulo: int >= 1

    :return: (int >= 0, int >= 0)
    """
    assert n >= 0
    assert modulo >= 1

    f_n_1 = 1
    f_n = 0
    while n:
        n -= 1
        f_n_1, f_n = f_n % modulo, (f_n + f_n_1) % modulo

    return f_n_1, f_n


def fibonacci_pair_iteration_memo(n: int, modulo: int) -> Tuple[int, int]:
    """
    Return the pair (F_{n-1} % modulo, F_n % modulo).

    Memoization of fibonacci_pair_iteration().

    :param n: int >= 0
    :param modulo: int >= 1

    :return: (int >= 0, int >= 0)
    """
    assert n >= 0
    assert modulo >= 1

    key = (n, modulo)
    result = MEMO.get(key)

    if result is None:
        result = fibonacci_pair_iteration(n, modulo)
        MEMO[key] = result

    return result


def fibonacci_sum(a: int, b: int, modulo: int) -> int:
    """
    Returns (F_a + F_{a+1} + F_{a+2} + ... F_b) modulo modulo.

    :param a: int >= 0
    :param b: int >= a
    :param modulo: int >= 1

    :return: int >= 0
    """
    assert 0 <= a <= b
    assert modulo >= 1

    f_sum = (fibonacci_pair(b + 2, modulo)[1] -
             fibonacci_pair(a + 1, modulo)[1])

    return f_sum % modulo


def main() -> None:
    """
    Read inputs, solve, and print result.
    """
    nb = int(input())

    assert 1 <= nb <= 30

    for _ in range(nb):
        # Read inputs
        a, b, d = map(int, input().split())

        assert 0 <= a <= b <= 1000000000
        assert b - a <= 10000000
        assert 1 <= d <= 1000000000

        # Compute F_{a-1} and F_a
        f_n_1, f_n = fibonacci_pair(a, d)

        # To check that "naive" solution is not good enough
        # f_n_1, f_n = fibonacci_pair_iteration(a, d)
        # f_n_1, f_n = fibonacci_pair_iteration_memo(a, d)

        # Compute sum (F_a + F_{a+1} + F_{a+2} + ... + F_b) modulo d
        sum_mod = f_n
        for _ in range(a + 1, b + 1):
            f_n_1, f_n = f_n, (f_n + f_n_1) % d
            sum_mod = (sum_mod + f_n) % d

        assert sum_mod == fibonacci_sum(a, b, d), (sum_mod,
                                                   fibonacci_sum(a, b, d))

        # Print result
        print('F_{} + ... + F_{} is {}divisible by {}'
              .format(a, b,
                      ('' if sum_mod == 0
                       else 'NOT '),
                      d))
        sys.stdout.flush()

    assert sys.stdin.readline() == ''


if __name__ == '__main__':
    main()
