/* -*- coding: utf-8 -*- */
/*
  Determine if some range of consecutive Fibonacci numbers are divisible.

  Read nb from standard input,
  then for each a b d from standard input
  determine if F_a + F_{a+1} + F_{a+2} + ... + F_b is divisible by d.

  GPLv3 --- Copyright (C) 2020, 2021 Olivier Pirson
  Olivier Pirson --- http://www.opimedia.be/
  January 9, 2021
*/

/* jshint esversion: 7 */
/* jshint node: true */
/* jshint strict: true */
/* jshint eqeqeq: true */

"use strict";


process.stdin.resume();
process.stdin.setEncoding("utf-8");


const lines = [];
let currentLine = 0;

process.stdin.on("data", text => {
    for (let line of text.split("\n")) {
        lines.push(line);
    }
});
process.stdin.on("end", main);


function readline() {
    if (currentLine >= lines.length) process.exit(1);

    return lines[currentLine++];
}



function square(n, modulo) {
    n %= modulo;

    return (n * n) % modulo;
}


function fibonacci_pair(n, modulo) {
    let f_n_1;
    let f_n;

    if (n <= 1) {
        f_n_1 = (n === 0
                 ? 1
                 : 0);
        f_n = n;
    }
    else {
        const q = Math.floor(n / 2);

        let f_q_1;
        let f_q;

        [f_q_1, f_q] = fibonacci_pair(q, modulo);

        if ((n & 1) === 0) {
            /*
              n = 2q
              q = n/2
              F_{n-1} = F_q^2 - F_{q-1}^2
              F_n = F_q (2 F_{q-1} + F_q)
            */
            f_n_1 = square(f_q, modulo) + square(f_q_1, modulo);
            f_n = f_q * ((f_q_1 * 2 % modulo + f_q) % modulo);
        }
        else {
            /*
              n = 2q + 1
              q = (n - 1)/2
              F_{n-1} = F_q (2 F_{q-1} + F_q)
              F_n = F_{q+1}^2 - F_q^2 = (F_q + F_{q-1})^2 - F_q^2
            */
            f_n_1 = f_q * ((f_q_1 * 2 % modulo + f_q) % modulo);
            f_n = square(f_q + f_q_1, modulo) + square(f_q, modulo);
        }
    }

    f_n_1 %= modulo;
    f_n %= modulo;

    return [f_n_1, f_n];
}


function main() {
    const nb = parseInt(readline());

    for (let i = 0; i < nb; ++i) {
        // Read inputs
        const inputs = readline().split(' ');
        const a = parseInt(inputs[0]);
        const b = parseInt(inputs[1]);
        const d = parseInt(inputs[2]);

        // Compute F_{a-1} and F_a
        let f_n_1;
        let f_n;

        [f_n_1, f_n] = fibonacci_pair(a, d);

        // Compute sum (F_a + F_{a+1} + F_{a+2} + ... + F_b) modulo d
        let sum_mod = f_n;

        for (let j = a + 1; j <= b; ++j) {
            const tmp = f_n_1;

            f_n_1 = f_n;
            f_n = (f_n + tmp) % d;

            sum_mod = (sum_mod + f_n) % d;
        }

        // Print result
        const divisible = (sum_mod === 0
                           ? ""
                           : "NOT ");

        console.log(`F_${a} + ... + F_${b} is ${divisible}divisible by ${d}`);
    }
}
