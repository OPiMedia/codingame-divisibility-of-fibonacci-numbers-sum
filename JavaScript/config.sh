# Config file for run.sh and foreach.sh (for solution program in JavaScript)
# See *List of configuration variables* in
# https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/

INPUT_PATTERN='../data/input/input*.txt'    # pattern of input files
OUTPUT_PATTERN='../data/correct/output*.txt'  # pattern of corresponding correct output files

VM='nodejs'  # virtual machine, to run PROG
VM_FLAGS=()

# ANALYZER=''  # dynamic analyzer, enabled by -z option
# ANALYZER_FLAGS=()

PROG='solution.js'  # main program
PROG_FLAGS=()

PROG_SRC='solution.js'  # source code

# INPUT_HEAD=0   # max number of lines to print, for input file
# RESULT_HEAD=0  # max number of lines to print, for result file
# OUTPUT_HEAD=0  # max number of lines to print, for output file

# MAKE_RUN=0  # if 0 then run make step

# DIFF='sdiff'      # side by side differences
# DIFF='colordiff'  # color diff
# DIFF_FLAGS=()

# DIFF_VIEW='kdiff3'  # differences viewer, enabled by -v or -V options
# DIFF_VIEW_FLAGS=()
