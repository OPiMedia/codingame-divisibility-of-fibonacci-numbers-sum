# Divisibility of Fibonacci numbers sum (CodinGame, September 22, 2020 - January 9, 2021)

Challenge for CodinGame:
https://www.codingame.com/training/hard/divisibility-of-fibonacci-numbers-sum



## Statement
```
<<Sum of consecutive big Fibonacci numbers is divisible or not?>>

Let [[a]], [[b]] and [[d]] non negative integers.
<<Determine if [[d]] divides F_[[a]] + F_{[[a]]+1} + F_{[[a]]+2} + … + F_[[b]].>>

The difficulty is that we ask that for <<some very big indices [[a]]>>.

Remember that the well-known Fibonacci numbers is a sequence of numbers starting with 0 and 1, and after that each number is the sum of the two previous ones.

F_0 = {{0}}
F_1 = {{1}}
F_{n+2} = F_{n+1} + F_n

So the first Fibonacci numbers are:
0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233…

<<Example:>>
[[a]] = 10
[[b]] = 12
F_10 + F_11 + F_12 = 55 + 89 + 144 = 288 = 32 * 9 = 2^5 * 3^2

This sum is divisible by [[d]] = 2, 4, 8, 16 and 32, but NOT by 64.
This sum is divisible by [[d]] = 3 and 9, but NOT by 27.
This sum is NOT divisible by [[d]] = 5.
This sum is divisible by [[d]] = 6.
…

Example of input:
`2
10 12 3
10 12 5`
And the expected output:
`F_10 + ... + F_12 is divisible by 3
F_10 + ... + F_12 is NOT divisible by 5`
(When [[a]] = [[b]] write in the same way the beginning "F_[[a]] + ... + F_[[b]]"
even if there is only one term.)

<<Hints:>>
Implementations of Fibonacci numbers computation is an usual example about recursivity and algorithmic complexity. The obvious way to implement it with recursion is a classical example of complexity explosion. There is several way to fix that, and the most obvious is the simple iteration that compute the result in a linear number of steps (if we consider that operations on numbers are in constant time, which is not really the case). The key point to solve this challenge is to find a mathematical way to be better than that and to implement it.
To help you can read the "Matrix form" section in "Fibonacci number" article on Wikipedia:
https://en.wikipedia.org/wiki/Fibonacci_number#Matrix_form

Maybe this list of the first 300 Fibonacci numbers and their factorization can help you:
http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Fibonacci/fibtable.html#100
But if you need these big numbers, you are probably on a wrong way. ;-)

Enjoy!

(If you are interested by the mathematical aspect, there exist a lot of nice properties about this sequence:
https://oeis.org/A000045 )

(The illustration image is a part of one page of the Liber Abaci:
https://commons.wikimedia.org/wiki/File:Liber_abbaci_magliab_f124r.jpg )
```


## Tags
Recursion, Loops, Arithmetic, Modular Calculus



## Difficulty
Difficult



## Inputs
```
<<Line 1:>> An non negative integer [[nb]] for the number of tests.

<<Next [[nb]] lines:>> Three space separated non negative integers [[a]], [[b]] and [[d]] for the first index, for the last index, and for the divisor.
```


## Outputs
```
<<[[nb]] lines:>> `"F_[[a]] + ... + F_[[b]] is divisible by [[d]]"` or `"F_[[a]] + ... + F_[[b]] is NOT divisible by [[d]]"`
```


## Constraints
```
{{1}} ≤ [[nb]] ≤ {{30}}
{{0}} ≤ [[a]] ≤ [[b]] ≤ {{1,000,000,000}}
[[b]] - [[a]] ≤ {{10,000,000}}
{{1}} ≤ [[d]] ≤ {{1,000,000,000}}
```



## Sub generator
```
read nb:int
loop nb read a:int b:int d:int
write answer
```



## Additional information
```
https://en.wikipedia.org/wiki/Fibonacci_number

To solve this challenge you will probably need of this beautiful formula:
F_{a+b} = F_a F_{b+1} + F_{a-1} F_b

F_{2n-1} = F_n^2 - F_{n-1}^2
F_{2n} = F_n (2 F_{n-1} + F_n)
F_{2n+1} = F_{n+1}^2 - F_n^2

F_0 + F_1 + F_2 + ... + F_n = F_{n+2} - 1

F_a + F_{a+1} + F_{a+2} + ... + F_b = F_{b+2} - F_{a+1}


Remember also that:
(a + b) modulo m = (a modulo m) + (b modulo m)


F_16 + F_17 + F_18 = 5168 = 2^4 * 17 * 19

F_102 = 927372692193078999176
F_21 = 10946
F_20 + F_21 + ... + F_100 = 927372692193078999176 - 10946
                          = 927372692193078988230
                          = 2 * 3 * 5 * 11 * 41 * 12743 * 1174951 * 4577887

F_123 + F_124 + ... + F_234 = 9366947731425726508977295269298648465332376891434
                            = 2 * 3 * 7^3 * 13 * 23 * 29 * 241 * 281
                              * 2161 * 8641 * 14503 * 20641 * 103681
                              * 13373763765986881
```


### Inputs
```
1. F_10 + F_11 + F_12 example: 10 12 ;
                 First values: 16 18
2. Other little values: 0 10, 5 16 ;
                        4 19, 0 12
3. Bigger values: 20 100, 32 85 ;
                  21 105, 23 106
4. Even bigger values: 110 301, 123 234, 211 398, 353 616, 432 433, 312 ;
                       102 305, 121 240, 345 602, 450 451, 354
5. Indices around 1,000: 1234 2345, 1781 2444, 2123 4489, 3560 4013, 3897 3910 ;
                         1111 2222, 1230 2343, 2156 4860, 3048 4482, 3604 6705
6. Indices around 10,000: 12345 23456, 12378 64506, 21568 554433, 28090 34708, 34890 40566, 40045 70000 ;
                          10347 60451, 12347 23458, 20570 54895, 34891 41254, 39800 60456
7. Indices below 1,000,000: 489 1005, 2468 897089, 48903 4680607, 654321 987654, 780613 980156 ;
                            815 2064, 3189 789015, 654317 987658, 777666 905436
8. Indices below 1,000,000,000: 123456789 123567900, 523489014 528904156, 987000000 987654321 ;
                                513489014 518904153, 987123456 987654441
9. Indices until 1,000,000,000: 42 4894511, 979000000 979002345, 999000000 1000000000, 1000000000 1000000000 ;
                                666 12345678, 978000000 978002345, 998000000 1000000000, 1000000000 1000000000
```



## Link
* [**HackerRank [CodinGame…] / helpers**](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/):
  little scripts to help in solving problems of HackerRank website (or CodinGame…)



## Author: 🌳 Olivier Pirson — OPi ![OPi][OPi] 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬

🌐 Website: <http://www.opimedia.be/>

💾 Bitbucket: <https://bitbucket.org/OPiMedia/>

- 📧 <olivier.pirson.opi@gmail.com>
- Mastodon: <https://mamot.fr/@OPiMedia> — Twitter: <https://twitter.com/OPirson>
- 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
- other profiles: <http://www.opimedia.be/about/>

[OPi]: http://www.opimedia.be/_png/OPi.png



## License: [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html) ![GPLv3](https://www.gnu.org/graphics/gplv3-88x31.png)

Copyright (C) 2020, 2021 Olivier Pirson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.



![Liber Abaci](https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2021/Jan/09/974567424-0-codingame-divisibility-of-fibonacci-nu_avatar.png)

(piece of a picture from [Liber Abaci](https://commons.wikimedia.org/wiki/File:Liber_abbaci_magliab_f124r.jpg))
