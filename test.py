#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Test functions of fibonacci.py.

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: October 23, 2020
"""

import sys

import search_divisor
import solution


def main() -> None:
    print('Start test... (compute during 1 minute on a Intel Xeon W3530 2.80GHz)')  # noqa

    assert solution.fibonacci_pair_iteration(0, modulo=1000000000) == (1, 0)
    assert solution.fibonacci_pair_iteration(1, modulo=1000000000) == (0, 1)
    assert solution.fibonacci_pair_iteration(2, modulo=1000000000) == (1, 1)
    assert solution.fibonacci_pair_iteration(3, modulo=1000000000) == (1, 2)
    assert solution.fibonacci_pair_iteration(4, modulo=1000000000) == (2, 3)
    assert solution.fibonacci_pair_iteration(5, modulo=1000000000) == (3, 5)
    assert solution.fibonacci_pair_iteration(10, modulo=1000000000) == (34, 55)
    assert solution.fibonacci_pair_iteration(20, modulo=1000000000) == (4181,
                                                                        6765)
    assert solution.fibonacci_pair_iteration(40,
                                             modulo=1000000000) == (63245986,
                                                                    102334155)

    assert search_divisor.fibonacci_pair(0) == (1, 0)
    assert search_divisor.fibonacci_pair(1) == (0, 1)
    assert search_divisor.fibonacci_pair(2) == (1, 1)
    assert search_divisor.fibonacci_pair(3) == (1, 2)
    assert search_divisor.fibonacci_pair(4) == (2, 3)
    assert search_divisor.fibonacci_pair(5) == (3, 5)
    assert search_divisor.fibonacci_pair(10) == (34, 55)
    assert search_divisor.fibonacci_pair(20) == (4181, 6765)
    assert search_divisor.fibonacci_pair(40) == (63245986, 102334155)

    for n in range(10000):
        if n % 1000 == 0:
            print('n = {}'.format(n))
            sys.stdout.flush()

        f_pair_iter = solution.fibonacci_pair_iteration(n, modulo=1000000000)
        f_pair = tuple(map(lambda v: v % 1000000000,
                           search_divisor.fibonacci_pair(n)))

        assert f_pair_iter == f_pair, (n, f_pair_iter, f_pair)

    for modulo in range(1, 100):
        if modulo % 10 == 0:
            print('modulo = {}'.format(modulo))
            sys.stdout.flush()

        for n in range(10000):
            f_pair = search_divisor.fibonacci_pair(n)
            f_pair_mod = solution.fibonacci_pair(n, modulo)

            assert f_pair[0] % modulo == f_pair_mod[0], (modulo, n,
                                                         f_pair, f_pair_mod)
            assert f_pair[1] % modulo == f_pair_mod[1], (modulo, n,
                                                         f_pair, f_pair_mod)

    print('End')


if __name__ == '__main__':
    main()
